import {Component} from 'react'
import {AtList, AtTabs, AtTabsPane} from 'taro-ui'
import {View} from '@tarojs/components'
import "taro-ui/dist/style/components/button.scss" // 按需引入
import "taro-ui/dist/style/components/tabs.scss";
import "taro-ui/dist/style/components/list.scss";
import "taro-ui/dist/style/components/icon.scss";
import './category.less'
import ApiService from "../../utils/service";
import CategoryCard from "../../components/category_card";
import {Category, Tag} from "../../model/blog";
import TagCard from "../../components/tag_card";

export default class CategoryIndex extends Component {

  state = {
    tags: [] as Tag[],
    cates: [] as Category[],
    current: 0
  }

  /// 在这里请求服务器数据
  async componentDidMount() {
    const _cates = await ApiService.getCategorys();
    const _tags = await ApiService.getTags();
    this.setState({
      cates: _cates,
      tags: _tags
    })
  }

  handleClick(value) {
    this.setState({
      current: value
    })
  }

  render() {
    const {cates, tags} = this.state;
    const tabList = [{title: '分类'}, {title: '标签'}, {title: '归档'}]
    const categorys = cates.map(value => <CategoryCard category={value} key={value.id}/>);
    const tagsList = tags.map(value => <TagCard tag={value} key={value.id}/>);
    return (
      <View className='index'>
        <AtTabs current={this.state.current} tabList={tabList} onClick={this.handleClick.bind(this)}>
          <AtTabsPane current={this.state.current} index={0}>
            <AtList>
              {categorys}
            </AtList>

          </AtTabsPane>
          <AtTabsPane current={this.state.current} index={1}>
            <View style={{padding: '5px'}}>
              {tagsList}
            </View>
          </AtTabsPane>
          <AtTabsPane current={this.state.current} index={2}>
            <View style='padding: 100px 50px;background-color: #FAFBFC;text-align: center;'>暂无归档数据</View>
          </AtTabsPane>
        </AtTabs>
      </View>
    )
  }
}
