import * as React from "react";
import {useState} from "react";
import {View} from "@tarojs/components";
// @ts-ignore
import Taro , {useReady} from "@tarojs/taro";
import ApiService from "../../utils/service";
import {Blog} from "../../model/blog";


// 博客详情页面
function Detail() {


   const [blog,setBlog] = useState<Blog|undefined>(null)

  useReady(async () => {
    const alias = Taro.getCurrentInstance().router.params.alias;
    const _blog = await ApiService.getBlogByAlias(alias);
    setBlog(_blog);
    console.log(_blog);
  })
  return <>
    {
      blog && <towxml nodes={'# 111'} />
    }
  </>
}

export default Detail;
