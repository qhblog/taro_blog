import {Component} from 'react'
// @ts-ignore
import Taro from '@tarojs/taro'
import {View, Text} from '@tarojs/components'
import {AtAvatar, AtList, AtListItem} from "taro-ui";
import "taro-ui/dist/style/components/button.scss" // 按需引入
import "taro-ui/dist/style/components/avatar.scss";
import "taro-ui/dist/style/components/flex.scss";
import "taro-ui/dist/style/components/icon.scss";
import "taro-ui/dist/style/components/list.scss";
// @ts-ignore
import ava from '../../res/ava.jpg'
import ApiService from "../../utils/service";
import {Statistics} from "../../model/statistics";


export default class Me extends Component {


  state = {
    statistics: null as Statistics
  }

  componentWillMount() {
  }

  /// 在这里请求服务器数据
  async componentDidMount() {
    const result = await ApiService.getStatistics();
    this.setState({
      statistics: result
    })
  }

  componentWillUnmount() {
  }

  componentDidShow() {
  }

  componentDidHide() {
  }

  render() {

    const {statistics} = this.state

    return (
      <View>
        <View style={{textAlign: 'center', marginTop: 12, width: '100%', borderBottom: '1px solid rgb(238, 238, 238)'}}>

          <AtAvatar circle image={ava} size='large'/>
          <View style={{
            marginBottom: 12,
            textAlign: "center"
          }}>
            <Text style={{
              fontSize: 30,
              fontWeight: 700,
            }}>梁典典的博客</Text>
          </View>
          <View style={{
            textAlign: 'center', padding: 20
          }}>
            <Text style={{
              fontSize: 13,
              color: '#696969',
              fontWeight: 400,
              textAlign: 'center',
              lineHeight: 1.65
            }}>95后程序员,当前在广州一家公司做全职Flutter开发,技术栈:Flutter,Dart,Android,Javascript,Kotlin,Java(服务端),小程序</Text>
          </View>

        </View>


        {/*  数据统计  */}
        {statistics ? <View>
          <View className='at-row'>
            <View className='at-col' style={{textAlign: 'center'}}>
              <View style={{padding: 12}}>
                <View><View className='at-icon at-icon-list' style={{color: 'blue'}}/> 博客</View>
                {statistics.blogCount} 篇
              </View>
            </View>
            <View className='at-col' style={{textAlign: 'center'}}>
              <View style={{padding: 12}}>
                <View><View className='at-icon at-icon-folder' style={{color: 'green'}}/> 分类</View>
                {statistics.cateCount} 个
              </View>
            </View>
            <View className='at-col' style={{textAlign: 'center'}}>
              <View style={{padding: 12}}>
                <View><View className='at-icon at-icon-tags' style={{color: 'red'}}/> 标签</View>
                {statistics.tagCount} 个
              </View>
            </View>
          </View>
        </View> : <View/>}

        {/*  更多操作  */}
        <View>

          <AtList>
            <AtListItem title='开源项目' arrow='right' onClick={async()=>{
              // 传入参数 id=2&type=test
              await Taro.navigateTo({
                url: '/pages/detail/detail?alias=test1'
              })

            }}/>
            <AtListItem title='联系方式' arrow='right'/>
            <AtListItem title='关于' arrow='right'/>
            <AtListItem title='更新记录' arrow='right'/>
            <AtListItem title='打赏' arrow='right'/>
          </AtList>

        </View>

      </View>
    )
  }
}
