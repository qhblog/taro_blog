
/// 数据统计的
export interface Statistics {
  blogCount: Number;
  cateCount: Number;
  tagCount: Number;
}
