import * as Taro from '@tarojs/taro'
import {baseUrl, Authorization} from "./app";

/// 服务器通用返回结果
type Result = {
  state: number;
  message: string;
  data: any
}

/// 返回结果处理
export interface ResposeHandle {
  onSuccess: (dataJson: any) => void,
  onError: (code: Number, message: String) => void
}

/// 发起一次get请求
const ApiGet = async (options = {
  data: {},
  url: '',
}, resultHandle: ResposeHandle) => {
  const response = await Taro.request({
    url: baseUrl + options.url,
    data: {
      ...options.data
    },
    header: {
      'Authorization': Authorization
    },
    method: "GET"
  })
  const data = response.data as Result;
  if (response.statusCode == 200) {
    resultHandle.onSuccess(data);
  } else {
    resultHandle.onError(data.state, data.message);
  }
}

export default ApiGet;
