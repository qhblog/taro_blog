import ApiGet from "./request";
import {Blog, Category, Tag} from "../model/blog";
import {Statistics} from "../model/statistics";

/**
 * 接口服务类
 */
class ApiService {

  /**
   * 获取博客分类列表
   */
  public static async getCategorys(): Promise<Category[]> {
    return new Promise<Category[]>((resolve, reject) => {
      ApiGet({url: '/api/blog/category-list', data: {}}, {
        onSuccess(data: any) {
          resolve(data.data as Category[]);
        },
        onError(code, msg) {
          console.log('获取分类失败:' + msg);
          reject({
            'code': code,
            'msg': msg
          });
        }
      });
    });
  }


  /**
   * 获取全部标签列表  tags
   */
  public static async getTags(): Promise<Tag[]> {
    return new Promise<Tag[]>(((resolve, reject) => {
      ApiGet({url: '/api/blog/tags', data: {}}, {
        onSuccess(data) {
          resolve(data.data as Tag[])
        },
        onError(code, msg) {
          console.log('获取标签失败');
          reject({
            'code': code,
            'msg': msg
          })
        }
      })
    }));
  }

  /// 获取数据统计信息
  public static async getStatistics(): Promise<Statistics> {
    return new Promise<Statistics>(((resolve, reject) => {
      ApiGet({url: '/api/blog/statistics', data: {}}, {
        onError(code: Number, message: String): void {
          reject({
            'code': code,
            'msg': message
          })
        }, onSuccess(dataJson: any): void {
          console.log(dataJson)
          resolve(dataJson.data as Statistics)
        }
      });
    }));
  }

  /// 根据分类id获取博客列表
  public static async getBlogListWithCategoryId(cateId: Number, page: Number, pageSize: Number): Promise<Blog[]> {
    return new Promise<Blog[]>(((resolve, reject) => {
      ApiGet({
        url: '/api/blog/category/blogs', data: {
          'categoryId': cateId,
          'page': page,
          'pageSize': pageSize
        }
      }, {
        onError(code: Number, message: String): void {
          reject({
            'code': code,
            'msg': message
          })
        }, onSuccess(dataJson: any): void {
          console.log(dataJson)
          resolve([])
        }
      })
    }))
  }

  /// 根据分类id获取博客列表
  public static async getBlogListWithTagId(tagId: Number, page: Number, pageSize: Number): Promise<Blog[]> {
    return new Promise<Blog[]>(((resolve, reject) => {
      ApiGet({
        url: '/api/blog/tag/blogs', data: {
          'tagId': tagId,
          'page': page,
          'pageSize': pageSize
        }
      }, {
        onError(code: Number, message: String): void {
          reject({
            'code': code,
            'msg': message
          })
        }, onSuccess(dataJson: any): void {
          console.log(dataJson)
          resolve([])
        }
      })
    }))
  }


  /**
   * 根据文章别名获取数据
   * @param alias
   */
  public static async getBlogByAlias(alias: string): Promise<Blog> {
    return new Promise<Blog>((resolve, reject) => {
      ApiGet({
        url: '/api/blog/alias', data: {
          'alias': alias
        }
      }, {
        onError(code: Number, message: String): void {
          reject({
            'code': code,
            'msg': message
          })
        }, onSuccess(dataJson: any): void {
          resolve(dataJson.data as Blog);
        }

      })
    });
  }


}

export default ApiService;
