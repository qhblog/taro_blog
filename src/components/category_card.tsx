import * as React from "react";
import {View} from "@tarojs/components";
import {AtListItem} from "taro-ui";
import "taro-ui/dist/style/components/list.scss";
import "taro-ui/dist/style/components/icon.scss";
import {Category} from "../model/blog";


/// 参数
type CategoryCardProps = {
  category: Category,
  onTap?: () => void
}


/// 分类卡片
const CategoryCard: React.FC<CategoryCardProps> = ({category}) => {
  return <>
    <AtListItem
      title={category.name}
      note={category.intro}
      arrow='right'
      thumb={category.logo}
    />
  </>
}
export default CategoryCard;
