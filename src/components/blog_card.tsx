import {Text, View} from "@tarojs/components";
import {AtTag} from "taro-ui";
import "taro-ui/dist/style/components/tag.scss";
import "taro-ui/dist/style/components/icon.scss";
import "taro-ui/dist/style/components/card.scss";
import * as React from "react";
import * as dayjs from "dayjs";
import {Blog} from "../model/blog";


/// 组件参数
type BlogProps = {
  blog: Blog
}

/// 博客卡片
const BlogCard: React.FC<BlogProps> = (props) => {
  const {blog} = props;
  return <>
    <View style={{
      boxShadow: 'rgb(0 0 0 / 4%) 0px 5px 40px',
      padding: '12px 0px',
      borderTop: '1px solid rgb(238, 238, 238)',
      borderBottom: '1px solid rgb(238, 238, 238)',
    }}>

      <View style={{padding: '0px 12px'}}>
        {/*标题*/}
        <View>
          <Text style={{fontSize: '25px', fontWeight: 700, color: '#111111'}}>{blog.title}</Text>
        </View>


        {/*发布时间*/}
        <View style={{
          marginTop: '12px',
          marginBottom: '12px',
        }}>
          <Text style={{
            color: '#696969',
            fontSize: '15px'
          }}>{dayjs(blog.dateString).format('YYYY/MM/DD')}</Text>
        </View>


        <AtTag><View className={'at-icon at-icon-folder'} style={{marginRight: '5px'}}/>{blog.category.name}</AtTag>
      </View>


    </View>
  </>;
}
export default BlogCard;
