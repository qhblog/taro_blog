import * as React from "react";
import {AtTag} from "taro-ui";
import "taro-ui/dist/style/components/tag.scss";
import {Tag} from "../model/blog";


type TagProps = {
  tag: Tag,
  onTap?: () => void
}


const TagCard: React.FC<TagProps> = ({tag, onTap}) => {
  return <>
    <AtTag circle onClick={onTap} customStyle={{marginRight:'3px',marginBottom:'4px'}}>{tag.name}</AtTag>
  </>
}
export default TagCard;
