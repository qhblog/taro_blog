export default {
  pages: [
    'pages/index/index',
    'pages/category/category',
    'pages/me/me',
    'pages/detail/detail',
  ],
  tabBar: {
    list: [{
      iconPath: 'res/home.png',
      selectedIconPath: 'res/home-filling.png',
      pagePath: 'pages/index/index',
      text: '首页',
    },
      {
        iconPath: 'res/folder.png',
        selectedIconPath: 'res/folder-filling.png',
        pagePath: 'pages/category/category',
        text: '分类'
      },
      {
        iconPath: 'res/user.png',
        selectedIconPath: 'res/user-filling.png',
        pagePath: 'pages/me/me',
        text: '我的'
      }
    ],
    'color': '#000',
    'selectedColor': '#56abe4',
    'backgroundColor': '#fff',
    'borderStyle': 'white'
  },
  window: {
    backgroundTextStyle: 'light',
    navigationBarBackgroundColor: '#fff',
    navigationBarTitleText: '典典博客',
    navigationBarTextStyle: 'black'
  }
}
